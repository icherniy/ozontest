using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using DAL;
using Xunit;

namespace OzonTest.Tests
{
    public class WebImageLoaderTests
    {
        [Fact]
        public void TestInvalidWebImageLoaderPatternsIsNull()
        {
            Assert.Throws<InvalidDataException>(() => new WebImageLoader(null));
        }

        [Fact]
        public void TestInvalidWebImageLoaderPatternsIsEmpty()
        {
            var imagePatterns = new List<string>();
            Assert.Throws<InvalidDataException>(() => new WebImageLoader(imagePatterns.ToArray()));
        }

        [Fact]
        public void TestWebImageLoaderUriIsNull()
        {
            var imagePatterns = new List<string>
            {
                "aasasa"
            };
            var webImageLoader = new WebImageLoader(imagePatterns.ToArray());
            Assert.Throws<InvalidDataException>(() => webImageLoader.Load(null));
        }

        [Fact]
        public void TestWebImageLoaderUriIsNotAvailable()
        {
            var imagePatterns = new List<string>
            {
                "aasasa"
            };
            var webImageLoader = new WebImageLoader(imagePatterns.ToArray());
            Assert.Throws<WebException>(() => webImageLoader.Load(new Uri("http://ewewedscxcssd.sdsdsd")));
        }
    }
}