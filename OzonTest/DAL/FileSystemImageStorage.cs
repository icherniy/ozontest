﻿using System;
using System.Collections.Generic;
using System.IO;
using DAL.Models;
using Serilog;

namespace DAL
{
    public class FileSystemImageStorage: IStorage
    {
        private readonly ILogger _logger = Log.ForContext<FileSystemImageStorage>();

        private readonly string _targetFolder;

        public FileSystemImageStorage(string targetFolder)
        {
            if (string.IsNullOrEmpty(targetFolder) || targetFolder.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
            {
                _logger.Error("Target folder is null or empty or has invalid chars.");
                throw new InvalidDataException("Target folder is null or empty or has invalid chars.");
            }

            _targetFolder = targetFolder;
        }

        public void Dispose()
        {
            // nothing to do.
        }

        public void Save(IEnumerable<Image> images)
        {
            if (images == null)
            {
                _logger.Error("Images is null.");
                throw new InvalidDataException("Images is null.");
            }

            if (!Directory.Exists(_targetFolder))
            {
                Directory.CreateDirectory(_targetFolder);
            }

            foreach (var image in images)
            {
                SaveImage(image);
            }

            _logger.Information($"Images were saved to {_targetFolder}.");
        }

        private void SaveImage(Image image)
        {
            var filename = Path.Combine(_targetFolder, string.Format($"{image.Name}{image.Extension}"));
            if (filename.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                _logger.Error($"Images filename {filename} is invalid.");
                throw new InvalidDataException($"Images filename {filename} is invalid.");
            }
            if (File.Exists(filename))
            {
                _logger.Warning($"Duplicates found: {filename}.");
                File.Delete(filename);
            }

            File.WriteAllBytes(filename, image.RawData);
        }

        public void CleanUp(string site)
        {
            _logger.Information("Cleanup started.");

            DirectoryInfo dirInfo = new DirectoryInfo(_targetFolder);

            foreach (FileInfo file in dirInfo.GetFiles())
            {
                file.Delete();
            }

            _logger.Information("Cleanup finished.");
        }
    }
}