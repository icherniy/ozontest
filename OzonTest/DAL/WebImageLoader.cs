﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DAL.Models;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Serilog;

namespace DAL
{
    public class WebImageLoader: IImageLoader
    {
        private readonly ILogger _logger = Log.ForContext<WebImageLoader>();


        private readonly string[] _imagePatterns;

        private Uri _datasourceUri;

        public WebImageLoader(string[] imagePatterns)
        {
            if (imagePatterns == null || imagePatterns.Length == 0)
            {
                _logger.Error("Image patterns is null or image pattern collection is empty.");
                throw new InvalidDataException("Image patterns is null or image pattern collection is empty.");
            }

            _imagePatterns = imagePatterns;
        }

        public ICollection<Image> Load(Uri datasourceUri)
        {
            if (datasourceUri == null)
            {
                _logger.Error("Data source url is null.");
                throw new InvalidDataException("Data source url is null.");
            }

            _datasourceUri = datasourceUri;
            using (WebClient client = new WebClient())
            {
                string html;
                try
                {
                    html = client.DownloadString(datasourceUri);
                }
                catch (Exception e)
                {
                    _logger.Fatal(e, "Source page download error.");
                    throw;
                }
                
                var imageLinks = BuildImageLinks(html);

                var imageLinksCount = imageLinks.Count;

                _logger.Information($"Found {imageLinksCount} image links.");

                if (imageLinksCount == 0)
                {
                    _logger.Information("Nothing to do.");
                }

                return DownloadImagesAsync(imageLinks).Result;
            }
        }

        private List<string> BuildImageLinks(string htmlCode)
        {
            var result = new List<string>();

            foreach (var pattern in _imagePatterns)
            {
                AddImages(result, pattern, htmlCode);
            }

            // remove doubles if needed.
            return result.Distinct().ToList();
        }

        private void AddImages(ICollection<string> result, string pattern, string htmlCode)
        {
            Regex r = new Regex(pattern, RegexOptions.IgnoreCase);
            Match m = r.Match(htmlCode);
            while (m.Success)
            {
                for (int i = 1; i <= m.Groups.Count; i++)
                {
                    Group g = m.Groups[i];
                    CaptureCollection cc = g.Captures;
                    for (int j = 0; j < cc.Count; j++)
                    {
                        var link = cc[j].ToString();
                        // Regex can return a quoted substring, which is allowed by the html syntax. 
                        // We must remove the quotes.
                        if (link.StartsWith("'") && link.EndsWith("'") ||
                            link.StartsWith("\"") && link.EndsWith("\""))
                        {
                            link = link.Substring(1, link.Length - 2);
                        }

                        result.Add(link);
                    }
                }

                m = m.NextMatch();
            }
        }

        private async Task<IList<Image>> DownloadImagesAsync(List<string> imageLinks)
        {
            var result = new List<Image>();

            foreach (var imageLink in imageLinks)
            {
                Uri uri;
                Image image;
                try
                {
                    image = new Image
                    {
                        Path = CorrectLinkIfNeeded(imageLink),
                        Site = _datasourceUri.ToString(),
                        Name = Path.GetFileNameWithoutExtension(imageLink)
                    };

                    if (string.IsNullOrEmpty(image.Name) || image.Name.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                    {
                        throw new InvalidDataException(image.Name);
                    }

                    image.Extension = Path.GetExtension(imageLink);
                    if (string.IsNullOrEmpty(image.Extension) ||
                        image.Extension.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
                    {
                        throw new InvalidDataException(image.Extension);
                    }

                    uri = new Uri(image.Path);
                }
                catch (Exception e)
                {
                    // do not fail if the image link could not be recognized
                    _logger.Warning(e, e.ToString());
                    continue;
                }

                using (var client = new WebClient())
                {
                    try
                    {
                        image.RawData = await client.DownloadDataTaskAsync(uri);
                         //client.DownloadData(uri);
                    }

                    // If the link is broken, skip
                    catch (Exception e)
                    {
                        _logger.Warning(e, e.ToString());
                        continue;
                    }
                    
                    image.Size = image.RawData.Length;
                }

                result.Add(image);
            }

           return result;
        }

        private string CorrectLinkIfNeeded(string link)
        {
            //limg.imgsmail.ru/splash/v/i/icons-fp-7959ce642c.png
            if (link.StartsWith("//"))
            {
                return "http:" + link;
            }
            
            // /sdsd/dd.jpg
            if (link.StartsWith("/") || !link.StartsWith("https") || !link.StartsWith("http") || !link.StartsWith("www"))
            {
                return new Uri(_datasourceUri, link).ToString();
            }

            return link;
        }
    }
}