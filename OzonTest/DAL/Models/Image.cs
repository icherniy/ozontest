﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    [Table("image")]
    public class Image
    {
        [Key, Column("id"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("site"), Required]
        public string Site { get; set; }

        [Column("name"), Required]
        public string Name { get; set; }

        [Column("extension"), Required]
        public string Extension { get; set; }

        [Column("path"), Required]
        public string Path { get; set; }

        [Column("size"), Required]
        public int Size { get; set; }

        [Column("raw_data"), Required]
        public byte[] RawData { get; set; }
    }
}