﻿using System;
using DAL.Models;
using System.Collections.Generic;

namespace DAL
{
    public interface IImageLoader
    {
        ICollection<Image> Load(Uri dataSourcUri);
    }
}