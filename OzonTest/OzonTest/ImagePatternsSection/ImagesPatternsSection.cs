﻿using System.Configuration;
using ConfigurationSection = System.Configuration.ConfigurationSection;

namespace OzonTest.ImagePatternsSection
{
    public class ImagesPatternsSection: ConfigurationSection
    {
        [ConfigurationProperty("Patterns")]
        public ImagesPatternsCollection ImagesPatternItems => (ImagesPatternsCollection)(base["Patterns"]);
    }
}