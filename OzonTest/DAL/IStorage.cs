﻿using System;
using System.Collections.Generic;
using DAL.Models;

namespace DAL
{
    public interface IStorage : IDisposable
    {
        void Save(IEnumerable<Image> images);

        void CleanUp(string site);
    }
}