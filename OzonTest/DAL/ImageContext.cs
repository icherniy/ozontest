﻿using System.Configuration;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ImageContext: DbContext
    {
        public DbSet<Image> Images { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options) =>
            options.UseNpgsql(ConfigurationManager.ConnectionStrings["PostgresConnectionString"].ConnectionString);
    }
}