﻿using System.Configuration;
using CommandLine;
using CommandLine.Text;

namespace OzonTest.CommandLine
{
    public class Options
    {
        [Option('u', "uri", Required = false, Default = "http://www.ozon.ru", HelpText = "Page address for downloading pictures.")]
        public string Uri { get; set; }

        [Option('d', "saveToDb", Required = false, HelpText = "Flag responsible for saving to the database. This key overrides the default value that is taken from application settings.")]
        public bool? SaveToDb { get; set; }

        [Option('c', "cleanUp", Required = false, Default = true, HelpText = "Flag responsible for clearing data before loading new ones.")]
        public bool CleanUp { get; set; }

        [Option('t', "targetFolder", Required = false, HelpText = "Destination folder for saving images. This parameter overrides the default value that is taken from application settings.")]
        public string TargetFolder { get; set; }
    }
}