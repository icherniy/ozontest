﻿using System.Configuration;

namespace OzonTest.ImagePatternsSection
{
    public class ImagePatternElement: ConfigurationElement
    {
        [ConfigurationProperty("patternValue", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string PatternValue
        {
            get => (string)base["patternValue"];
            set => base["patternValue"] = value;
        }
    }
}