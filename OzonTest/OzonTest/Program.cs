﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Autofac;
using CommandLine;
using DAL;
using OzonTest.CommandLine;
using OzonTest.ImagePatternsSection;
using Serilog;

namespace OzonTest
{
    class Program
    {
        private static readonly ICollection<string> _patterns = new List<string>();

        private static ILogger _logger;

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();
            _logger = Log.ForContext<Program>();
            _logger.Information("Work started.");

            ReadImagePatterns();

            try
            {
                Parser.Default.ParseArguments<Options>(args)
                    .WithNotParsed(errs => {
                        _logger.Error("An error occurred while parsing the command line parameters.");
                        ShowErrors(errs);
                    })
                    .WithParsed(ProcessCommandLineParameters);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.ToString());
            }
            finally
            {
                _logger.Information("Work finished.");
                Console.ReadLine();
            }
        }

        private static void ProcessCommandLineParameters(Options opt)
        {
            _logger.Information($"Page source is {opt.Uri}.");

            using (var container = ConfigureComponents(opt))
            {
                var manager = container.Resolve<IImageManager>();
                
                manager.Load(new Uri(opt.Uri));

                manager.Save(opt.CleanUp);
            }
        }

        private static void ReadImagePatterns()
        {
            ImagesPatternsSection section = (ImagesPatternsSection)ConfigurationManager.GetSection("ImagesPatterns");

            if (section != null)
            {
                foreach (ImagePatternElement item in section.ImagesPatternItems)
                {
                    _patterns.Add(item.PatternValue);
                }
            }
        }

        private static IContainer ConfigureComponents(Options options)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<WebImageLoader>().As<IImageLoader>().
                WithParameter("imagePatterns", _patterns.ToArray());
            var saveToDb = options.SaveToDb ?? bool.Parse(ConfigurationManager.AppSettings["saveToDb"]);
            if (saveToDb)
            {
                builder.RegisterType<PostgresImageStorage>().As<IStorage>();
            }
            else
            {
                builder.RegisterType<FileSystemImageStorage>().As<IStorage>()
                    .WithParameter("targetFolder", GetTargetFolder(options));
            }

            builder.RegisterType<ImageManager>().As<IImageManager>();

            return builder.Build();
        }

        private static string GetTargetFolder(Options options)
        {
            var generalTargetFolder =  !string.IsNullOrEmpty(options.TargetFolder) ? options.TargetFolder : 
                                        ConfigurationManager.AppSettings["targetFolder"];

            return Path.Combine(generalTargetFolder, new Uri(options.Uri).Host);
        }

        private static void ShowErrors(IEnumerable<Error> errors)
        {
            foreach (var error in errors)
            {
                _logger.Error(error.Tag.ToString());
            }
        }
    }
}