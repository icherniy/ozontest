﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;

namespace DAL
{
    public class PostgresImageStorage: IStorage
    {
        private readonly ILogger _logger = Log.ForContext<PostgresImageStorage>();

        private readonly ImageContext _db;

        public PostgresImageStorage()
        {
            _db = new ImageContext();
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save(IEnumerable<Image> images)
        {
            _db.Images.AddRange(images);
            _db.SaveChanges();

            _logger.Information("Images were saved to database.");
        }

        public void CleanUp(string dataSourceUri)
        {
            _logger.Information("Cleanup started.");

            var imagesToDelete = _db.Images.Where(image => image.Site == dataSourceUri.ToString());
            if (imagesToDelete.Any())
            {
                _db.RemoveRange(imagesToDelete);
                _db.SaveChanges();
            }

            _logger.Information("Cleanup finished.");
        }
    }
}