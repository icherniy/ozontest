﻿using System;
using System.Collections.Generic;
using DAL.Models;
using Serilog;

namespace DAL
{
    public class ImageManager: IImageManager
    {
        private readonly ILogger _logger = Log.ForContext<ImageManager>();

        private readonly IImageLoader _imageLoader;

        private readonly IStorage _storage;

        private ICollection<Image> _images;

        private string _site;

        public ImageManager(IImageLoader imageLoader, IStorage storage)
        {
            _imageLoader = imageLoader;
            _storage = storage;
        }

        public void Load(Uri uri)
        {
            _site = uri.Host;
            _images = _imageLoader.Load(uri);

            _logger.Information($"Downloaded {_images.Count} images.");
        }

        public void Save(bool cleanUp)
        {
            if (_images == null)
            {
                _logger.Error("Load images first.");
                return;
            }

            if (cleanUp)
            {
                _storage.CleanUp(_site);
            }

            _storage.Save(_images);
        }
    }
}