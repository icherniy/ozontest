﻿using System.Configuration;

namespace OzonTest.ImagePatternsSection
{
    [ConfigurationCollection(typeof(ImagePatternElement))]
    public class ImagesPatternsCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ImagePatternElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ImagePatternElement)(element)).PatternValue;
        }

        public ImagePatternElement this[int idx] => (ImagePatternElement)BaseGet(idx);
    }
}