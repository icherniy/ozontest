﻿using System;

namespace DAL
{
    public interface IImageManager
    {
        void Load(Uri uri);

        void Save(bool cleanUp);
    }
}