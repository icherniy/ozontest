using System;
using System.IO;
using DAL;
using Xunit;

namespace OzonTest.Tests
{
    public class FileSystemImageStorageTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("~+\\   |/df")]
        public void TestInvalidFileSystemImageStorageTargetFolder(string targetFolder)
        {
            Assert.Throws<InvalidDataException>(() => new FileSystemImageStorage(targetFolder));
        }

        [Fact]
        public void TestFileSystemImageStorageSaveNull()
        {
            var flieSystemStorage = new FileSystemImageStorage("d:\\DownloadedImages");
            Assert.Throws<InvalidDataException>(() => flieSystemStorage.Save(null));
        }
    }
}